# Gitlab ADR Tools

A command line tool to manage [ADRs](https://adr.github.io/) in gitlab issues.

## Workflow

With this tool, ADRs are recorded in the issues of a Gitlab project. The following workflow must be used for the tool to function correctly (we plan to add the ability to customize it in the future)

* Each ADR is written in an issue with the "ADR" label.
* An ADR can have the following status, each status with a corresponding label
    * Proposal (label: `ADR Status::proposal`)
    * Accepted (label: `ADR Status::accepted`)
    * Rejected (label: `ADR Status::rejected`)
    * Superseded (label: `ADR Status::superseded`)
* When an ADR is accepted, it can make a previous ADR obsolete. This is called *superseding*. In this case, the label `ADR Status::superseded` is applied to the old ADR, and bot adr will include a text referencing the other one (Superseded by #x, Supercedes #x)
* The Open/Closed status of the issue is not important for the tool, but we suggest keeping the ADRs open while they need visibility for discussion (most often it will be while they are in the proposal status).

## Commands

### Create a new ADR

```bash
adr new <title>
```

This will open your browser at the issue creation form. The form will be prefilled with the title and an ADR template.

This allows you to write the ADR using the gitlab editor and markdown preview.

When you submit the issue, the correct labels will be applied (`ADR` and `ADR Status::proposal`)

### List ADRs

```bash
adr list [--open]
```

Prints a list of all ADRs in the project, along with their issue ID.

if `--open` is specified, the browser is opened instead on the issues list page, filtered to display only the ADR issues. 

### Accept / Reject an ADR

```bash
adr accept/reject [--supersedes <old_adr_id>] <id>
```

Changes the status of an ADR. 

If the `reject` command is used, the ADR `<id>` will have its status label changed to `ADR Status::rejected`.

If the `accept` command is used and the `--supersedes` option is absent, the ADR `<id>` will have its status label changed to `ADR Status::accepted`.

If the `accept` command is used and the `--supersedes` option is presend, the ADR `<id>` will have its status label changed to `ADR Status::accepted` and the ADR `<id>` will have its status label changed to `ADR Status::superseded`. Both ADRs will be updated so that their description includes a link to the other.

## Prerequisites

To help check and setup the prerequisites, use the `adr init` command.

### Location

All the commands must be run in a git repository with at least one remote configured to a gitlab project. This gitlab project will be the one where the ADRs are created.

If there are several gitlab remotes, the first one will be used.

### Project

The following labels must exist on the project in which you want to store ADRs : 

* `ADR`
* `ADR Status::proposal`
* `ADR Status::accepted`
* `ADR Status::rejected`
* `ADR Status::superseded`

### Authentication

Some commands need to use authenticated API endpoints. You need to have Reporter access to the project in which you want to store ADRs