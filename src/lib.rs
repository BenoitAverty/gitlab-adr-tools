use std::error::Error;

pub mod input;
mod adr;
mod gitlab;
mod run;

pub use run::run;

type Result<T> = std::result::Result<T, Box<dyn Error>>;
