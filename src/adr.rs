use std::process::ExitStatus;
use std::io;

use url::form_urlencoded::Serializer;
use crate::{
    gitlab::Issue,
    input::GitlabSlug,
    Result
};

pub fn open_create(project_slug: &GitlabSlug, title: &str) -> io::Result<ExitStatus> {
    static MADR_TEMPLATE: &'static str = include_str!("madr_template.md");

    let url = format!("http://gitlab.com/{}/{}/issues/new?", project_slug.namespace, project_slug.project);

    // Here using an Url instead of a query param Serializer would probably be better, but
    // for some reason the carriage returns were lost during percent encoding, whereas this method
    // correctly keeps them as %0A / %0D
    let url: String = Serializer::new(url)
        .append_pair("issue[description]", MADR_TEMPLATE)
        .append_pair("issue[title]", title)
        .finish();

    open::that(url)
}

pub async fn list(project_slug: &GitlabSlug) -> Result<String> {

    let issues = Issue::get_for_project(project_slug).await?;

    Ok(issues.into_iter()
        .filter(Issue::is_adr)
        .map(|i| i.to_terminal_list_item())
        .collect::<Vec<_>>()
        .join("\n"))
}

pub async fn approve(project_slug: &GitlabSlug, adr_id: u32, token: String) -> Result<()> {

    let issue = Issue::get_for_project_and_id(project_slug, adr_id).await?;

    Ok(issue.approve(project_slug, &token).await?)
}