use crate::{
    input::commands::Command::{New, List, Approve},
    Result
};

#[derive(Debug)]
pub enum Command {
    New(String),
    Approve(u32),
    List,
}

impl Command {
    pub fn parse<T: Iterator<Item = String>>(command: String, mut args: T) -> Result<Self> {
        match &command[..] {
            "new" => Ok(New(args.collect::<String>())),
            "approve" => {
                let adr_id: u32 = args.next().ok_or("No adr ID given")?.parse()?;
                Ok(Approve(adr_id))
            },
            "list" => Ok(List),
            c => {
                Err(Box::from(format!("command {} doesn't exist", c)))
            }
        }
    }
}