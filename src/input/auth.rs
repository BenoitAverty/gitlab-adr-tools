use crate::input::Command;
use crate::input::commands::Command::Approve;
use crate::Result;

pub fn needs_authentication(command: &Command) -> bool {
    if let Approve(_) = command {
        true
    } else {
        false
    }
}

pub async fn authenticate() -> Result<String> {
    let token: String  = rpassword::prompt_password_stdout("This command needs authentication. Please enter your gitlab token : ")?;

    Ok(token)
}