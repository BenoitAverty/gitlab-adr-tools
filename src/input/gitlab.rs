use git2::{Repository, Remote};

#[derive(Debug)]
pub struct GitlabSlug {
    pub namespace: String,
    pub project: String,
}

pub fn build_gitlab_project_slug(repo: &Repository, remote_name: Option<&str>) -> Option<GitlabSlug> {
    remote_name
        .and_then(|rn| repo.find_remote(rn).ok())
        .filter(|r| r.url().is_some())
        .map(build_slug)
}

fn build_slug(r: Remote) -> GitlabSlug {
    let url = r.url().unwrap();
    let slug =
        if url.contains(":") {
            build_slug_from_git(url)
        } else {
            build_slug_from_http(url)
        };

    let parts: Vec<&str> = slug.split("/").collect();
    GitlabSlug {
        namespace: String::from(*parts.get(0).unwrap()),
        project: String::from(*parts.get(1).unwrap()),
    }
}

fn build_slug_from_git(url: &str) -> &str {
    url.trim_start_matches("git@gitlab.com:")
        .trim_end_matches(".git")
}

fn build_slug_from_http(url: &str) -> &str {
    url.trim_start_matches("http://")
        .trim_start_matches("https://")
        .trim_start_matches("gitlab.com/")
        .trim_end_matches(".git")
}
