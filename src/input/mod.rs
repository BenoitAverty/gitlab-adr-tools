use std::error::Error;

mod gitlab;
mod commands;
mod auth;

pub use commands::Command;
pub use gitlab::GitlabSlug;

#[derive(Debug)]
pub struct InputConfig {
    pub executable: String,
    pub command: Command,
    pub gitlab_slug: GitlabSlug,
    pub token: String,
}

impl InputConfig {
    pub async fn gather() -> Result<InputConfig, Box<dyn Error>> {
        // Gather from command line arguments
        let mut args = std::env::args();

        let executable = args.next().ok_or("Not enough arguments")?;
        let command = args.next().ok_or("No command specified")?;
        let command = Command::parse(command, args)?;

        let token = if auth::needs_authentication(&command) {
            auth::authenticate().await?
        } else { String::new() };

        // Find the gitlab repo
        let current_working_dir = std::env::current_dir()?;
        let gitlab_repo = git2::Repository::discover(current_working_dir)?;
        let gitlab_repo = gitlab_repo
            .remotes()?
            .iter()
            .filter_map(|r| gitlab::build_gitlab_project_slug(&gitlab_repo, r))
            .nth(0)
            .ok_or("Could not find the current gitlab repo")?;


        Ok(InputConfig {
            executable,
            command,
            gitlab_slug: gitlab_repo,
            token,
        })
    }
}


