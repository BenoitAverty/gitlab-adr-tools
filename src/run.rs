use crate::input::{InputConfig, Command};
use crate::adr;
use crate::Result;

pub async fn run(config: InputConfig) -> Result<String> {
    match config.command {
        Command::New(title) => {
            adr::open_create(&config.gitlab_slug, &title)?;
            Ok(String::from("opened adr creation form"))
        }
        Command::Approve(adr_id) => {
            adr::approve(&config.gitlab_slug, adr_id, config.token).await?;
            Ok(format!("Approved {} successfully", adr_id))
        }
        Command::List => {
            let output = adr::list(&config.gitlab_slug).await?;
            Ok(output)
        }
    }
}
