use std::process::exit;

use gitlab_adr_tools::input::InputConfig;
use gitlab_adr_tools::run;

#[tokio::main]
async fn main() {
    let user_input = InputConfig::gather().await
        .unwrap_or_else(|e| usage(None, Some(&e)));

    let result = run(user_input).await;
    match result {
        Ok(output) => {
            println!("{}", output);
            exit(0);
        }
        Err(e) => {
            eprintln!("{}", &e);
            exit(1);
        }
    }
}

fn usage(executable: Option<&str>, message: Option<&dyn ToString>) -> ! {
    let executable = executable.unwrap_or("gitlab-adr-tools");
    let mut usage_str = String::new();

    match message {
        Some(m) => usage_str.push_str(&m.to_string()),
        None => usage_str.push_str("Gitlab ADR Tools : Manage architecture decision records stored in gitlab issues")
    };
    usage_str.push_str("\n\n");
    usage_str.push_str("Usage: ");
    usage_str.push_str(executable);
    usage_str.push_str(" <command>");

    eprintln!("{}", usage_str);

    std::process::exit(1)
}