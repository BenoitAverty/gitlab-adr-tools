use serde::Deserialize;
use crate::{
    Result,
    input::GitlabSlug
};
use url::form_urlencoded::Serializer;

#[derive(Deserialize, Debug)]
pub struct Issue {
    iid: u32,
    pub title: String,
    pub labels: Vec<String>,
}

impl Issue {
    pub async fn get_for_project(project: &GitlabSlug) -> Result<Vec<Issue>> {
        let uri = format!("https://gitlab.com/api/v4/projects/{}%2F{}/issues", project.namespace, project.project);

        let response = reqwest::get(&uri).await?;
        Ok(response.json::<Vec<Issue>>().await?)
    }

    pub async fn get_for_project_and_id(project: &GitlabSlug, iid: u32) -> Result<Issue> {
        let uri = format!("https://gitlab.com/api/v4/projects/{}%2F{}/issues/{}", project.namespace, project.project, iid);

        let response = reqwest::get(&uri).await?;
        Ok(response.json::<Issue>().await?)
    }

    pub async fn approve(&self, project: &GitlabSlug, token: &str) -> Result<()> {
        // Here using an Url instead of a query param Serializer would probably be better, but
        // for some reason the carriage returns were lost during percent encoding, whereas this method
        // correctly keeps them as %0A / %0D
        let uri = format!("https://gitlab.com/api/v4/projects/{}%2F{}/issues/{}/notes?", project.namespace, project.project, self.iid);
        let uri: String = Serializer::new(uri)
            .append_pair("body", "This ADR has been approved.\n/label ~\"ADR Status::accepted\"\n/close")
            .finish();
        let client = reqwest::Client::new();
        let result = client.post(&uri)
            .header("PRIVATE-TOKEN", token)
            .send().await?;
        dbg!(token);
        dbg!(result.text().await?);
        Ok(())
    }

    pub fn is_adr(&self) -> bool {
        self.labels.iter().any(|label| label == "ADR")
    }

    pub fn to_terminal_list_item(&self) -> String {
        format!("\t* {} (#{})", self.title, self.iid)
    }
}